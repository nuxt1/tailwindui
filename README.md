[![Netlify Status](https://api.netlify.com/api/v1/badges/aed1c8ff-40db-4568-8b98-1168dfcd907b/deploy-status)](https://app.netlify.com/sites/taiwindui/deploys)


https://taiwindui.netlify.app/


# nuxt-tailwindui

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# install taiwindui
update package.json
update nuxt.config.js

# create taiwindconfig.js
npx tailwindcss init
update tailwind.config.js

# add tailwindui example
update page/index.vue



